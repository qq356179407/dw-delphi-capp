unit ulog;

interface
           
uses
  SysUtils, ADODB, IniFiles, Forms;

type
  PLog = ^TLog;
  TLog = class
  private
    fstrfilepre : string;
  public
    procedure appendlog(strlog : string);
    //增加日志
    procedure appendWarning(strlog : string);
    //添加警告日志
    procedure appendError(strlog : string);
    //添加错误日志

  public
    constructor Create(strFilePre : string);
    //strFilePre---日志文件前缀
  
  end;
  
var
  log : TLog;
  
implementation

{ TLog }

procedure TLog.appendError(strlog: string);
var
  filename : string;
  strline : string;
  F: TextFile;
begin
  filename := ExtractFilePath(application.ExeName) + '\log\' + fstrfilepre + DateToStr(Now) + '.log';
  AssignFile(F, filename);
  if not FileExists(filename) then
  begin
    //reset(F);
    Rewrite(F)
  end
  else
  begin
    reset(F);
    Append(F);
  end;
  strline := TimeToStr(Now) + ':   ERROR:' + strlog;

  Writeln(F, strline);
  CloseFile(F);
end;

procedure TLog.appendlog(strlog: string);
var
  filename : string;
  strline : string;
  F: TextFile;
begin
  filename := ExtractFilePath(application.ExeName) + '\log\' + fstrfilepre + DateToStr(Now) + '.log';
  AssignFile(F, filename);
  if not FileExists(filename) then
  begin      
    //reset(F);
    Rewrite(F);
  end
  else
  begin
    reset(F);
    Append(F);
  end;

  strline := TimeToStr(Now) + ':   ' + strlog;
  Writeln(F, strline);
  CloseFile(F);
end;

procedure TLog.appendWarning(strlog: string);
var
  filename : string;
  strline : string;
  F: TextFile;
begin
  filename := ExtractFilePath(application.ExeName) + '\log\' + fstrfilepre + DateToStr(Now) + '.log';
  AssignFile(F, filename);
  if not FileExists(filename) then
  begin
    //Reset(F);
    Rewrite(F);
  end
  else
  begin
    Reset(F);
    Append(F);
  end;
  
  strline := TimeToStr(Now) + ':   WARNING:' + strlog;
  Writeln(F, strline);
  CloseFile(F);
end;

constructor TLog.Create(strFilePre: string);
begin
  fstrfilepre := strFilePre;
end;

end.
