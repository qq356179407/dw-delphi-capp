unit ufrmmain;

interface
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ufrmbase, StdCtrls, ExtCtrls, DB, ADODB, uencode, udbconfigfile,
  CnHint, Menus, UpPopupMenu, UPanel, UpLabel, Buttons,
  UpSpeedButton;

type
  Tfrmmain = class(Tfrmbase)
    adocon: TADOConnection;
    sdig: TSaveDialog;
    ptop: TUPanel;
    pleft: TUPanel;
    lbl1: TLabel;
    edtserver: TEdit;
    lbl2: TLabel;
    edtuser: TEdit;
    lbl3: TLabel;
    edtpassword: TEdit;
    lbl4: TLabel;
    edtdatabase: TEdit;
    lblw1: TUpLabel;
    lblw2: TUpLabel;
    lblw3: TUpLabel;
    upmsaveconfig: TUpPopupMenu;
    mnsavedefault: TMenuItem;
    mnsaveas: TMenuItem;
    btnsave: TUpSpeedButton;
    btnt: TUpSpeedButton;
    pmtitle: TPopupMenu;
    procedure mnsaveasClick(Sender: TObject);
    procedure mnsavedefaultClick(Sender: TObject);
    procedure btntClick(Sender: TObject);
  private
    procedure pmtitle_menuItemOnClick(Sender : TObject);
  public
    { Public declarations }
  end;

var
  frmmain: Tfrmmain;

implementation

{$R *.dfm}

procedure Tfrmmain.mnsaveasClick(Sender: TObject);  
var
  str : string;
  len : longint;
  buf : array[0..1024] of char;
  buffer : array[0..1024] of char;
  dbconfigfile : TDBConfigFile;
  strencode : TEncode;
begin
  inherited;
  try        
    zeromemory(@buf, 1024);
    zeromemory(@buffer, 1024);
    sdig.FileName := '';
    
    if not sdig.Execute then
      exit;
      
    str := 'Provider=SQLOLEDB.1;Persist Security Info=True;' +
           'Data Source=' + edtserver.Text + ';' +
           'Initial Catalog=' + edtdatabase.Text + ';' +
           'User ID=' + edtuser.Text + ';' +
           'Password=' + edtpassword.Text;

    if fileexists(sdig.FileName) then
    begin
      if not baseobj.showdialog('系统提示', '文件' + sdig.FileName + '已经存在，确认覆盖吗？') then
      begin
        baseobj.showmessage('操作已取消！');
        exit;
      end;
    end;

    len := length(str);
    dbconfigfile := TDBConfigFile.create(sdig.FileName);
    dbconfigfile.writeconfigstring(pchar(str)^, len);
    
    freeandnil(dbconfigfile);

    baseobj.showmsg('数据库连接参数配置', '参数配置成功', 0);
  except
    on e:exception do
    begin
      baseobj.showmsg('写数据库连接文件', '发生未知错误：' + e.Message, 0);      
    end
  end;
end;

procedure Tfrmmain.mnsavedefaultClick(Sender: TObject);   
var
  str : string;
  len : longint;
  buf : array[0..1024] of char;  
  buffer : array[0..1024] of char;
  dbconfigfile : TDBConfigFile;
  strencode : TEncode;
  dbconfigfilename : string;
begin
  { 合法性检查
  }
  if Trim(edtserver.Text) = '' then
  begin
    baseobj.showwarning('服务名不得为空！');
    edtserver.SetFocus;
    Exit;
  end;    
  if Trim(edtuser.Text) = '' then
  begin
    baseobj.showwarning('数据库用户不得为空！');
    edtuser.SetFocus;
    Exit;
  end;
  if Trim(edtpassword.Text) = '' then
  begin
    baseobj.showwarning('数据库口令不得为空！');
    edtpassword.SetFocus;
    Exit;
  end;
  if Trim(edtdatabase.Text) = '' then
  begin
    baseobj.showwarning('数据库名称不得为空！');
    edtdatabase.SetFocus;
    Exit;
  end;

  dbconfigfilename := ExtractFilePath(Application.ExeName) + '\dbconnfile.ini';
  zeromemory(@buf, 1024);
  zeromemory(@buffer, 1024);
  try      
    str := 'Provider=SQLOLEDB.1;Persist Security Info=True;' +
           'Data Source=' + edtserver.Text + ';' +
           'Initial Catalog=' + edtdatabase.Text + ';' +
           'User ID=' + edtuser.Text + ';' +
           'Password=' + edtpassword.Text;  

    len := length(str);
    dbconfigfile := TDBConfigFile.create(dbconfigfilename);
    dbconfigfile.writeconfigstring(pchar(str)^, len);
    
    freeandnil(dbconfigfile);

    baseobj.showmsg('数据库连接参数配置', '参数配置成功', 0);
  except
    on e:exception do
    begin
      baseobj.showmsg('写数据库连接文件', '发生未知错误：' + e.Message, 0);      
    end
  end;
end;

procedure Tfrmmain.btntClick(Sender: TObject);
var
  str : string;
begin
  inherited;
  try
    {
      Provider=SQLOLEDB.1;Password=12;Persist Security Info=True;User ID=12;Initial Catalog=db;Data Source=12323
    }
    str := 'Provider=SQLOLEDB.1;Persist Security Info=True;' +
           'Data Source=' + edtserver.Text + ';' +
           'Initial Catalog=' + edtdatabase.Text + ';' +
           'User ID=' + edtuser.Text + ';' +
           'Password=' + edtpassword.Text;
    if adocon.Connected then
      adocon.Close;

    adocon.ConnectionString := str;
    adocon.Open;
    adocon.Close;
    ShowMessage('数据库连接测试成功');
  except
    on e:exception do
    begin
      ShowMessage('连接失败：' + e.Message);
    end;
  end;
end;

procedure Tfrmmain.pmtitle_menuItemOnClick(Sender: TObject);

begin
end;

end.
