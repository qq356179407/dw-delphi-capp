unit udmoperator;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, udmtblbase, ADODB, UpAdoStoreProc, DB, UpAdoQuery, uglobal,
  udlltransfer;

type
  Tdmoperator = class(Tdmtblbase)
  public
    ioperid : Integer;
    //用户编码

    function checkPassword(bianm : integer; passstr : string) : boolean;

    function updatePassword(bianm : Integer; newpassword : string) : Boolean;
    //修改口令
  public
    function getusernamebyuserid(userid : string; var UserName : string) : Boolean;
    //根据用户id获取用户姓名

    function password_check(userid,password : string) : Boolean;
    //用户口令验证
    function auth_type_db(userid,password : string) : boolean;
    //db认证
    
  public
    constructor Create(pdllparams : PDllParam); override;
    destructor Destroy; override;
  end;

var
  dmoperator: Tdmoperator;

implementation

uses uDmBase;

{$R *.dfm}

{ Tdmoperator }

function Tdmoperator.auth_type_db(userid, password: string): boolean;
begin
  result := false;

  //普通用户登录验证
  qry1.OpenSql('select * from jc_yuang ' +
    ' where denglzh=''' + userid + '''' +
    '   and koul=''' + password + '''');
  if qry1.RecordCount>0 then
  begin
    ioperid := qry1.getInteger('bianm');
    Result := True;
  end
  else
  begin
    errmsg := '错误的操作员口令！';  
  end;
  qry1.Close;
end;


function Tdmoperator.checkPassword(bianm: integer;
  passstr: string): boolean;
begin
  if bianm=0 then
  begin
    //超级管理员
    qry1.OpenSql(format('select cansz from sys_xitcs where cansmc=''g_ap''',[]));
    if qry1.isEmpty then Result:= false;
    if qry1.getString(0)<>passstr then
      Result:= false
    else
      result:= true;
    qry1.Close;
  end
  else
  begin                                  
    qry1.OpenSql('select top 1 1 from jc_yuang ' +
      ' where bianm=' + IntToStr(bianm) +
      '   and koul=''' + passstr + '''');
    Result := qry1.RecordCount > 0;
    qry1.Close;
  end;
end;

constructor Tdmoperator.Create(pdllparams: PDllParam);
begin
  inherited Create(pdllparams);

end;

destructor Tdmoperator.Destroy;
begin
    
  inherited;
end;

function Tdmoperator.getusernamebyuserid(userid: string;
  var UserName: string): Boolean;
begin
  Result := False;

  //是否超级管理员，特殊处理
  if userid = 'admin' then
  begin
    UserName := '系统管理员';
    Result := True;
    Exit;
  end;
  
  qry1.OpenSql('select * from jc_yuang where denglzh=''' + userid + '''');
  if qry1.RecordCount<=0 then
  begin
    errmsg := '未找到员工信息！';
    qry1.Close;
    Exit;
  end;

  UserName := qry1.getString('xingm');
  qry1.Close;
  
  Result := True;
end;

function Tdmoperator.password_check(userid, password: string): Boolean;
var
  authtype : string;
begin
  Result := False;

  { 超级管理员db认证
  }
  //检查是否是超级管理员
  if(userid = 'admin') then
  begin
    qry1.OpenSql('select * from sys_xitcs ' +
      ' where cansmc=''g_ap''' +
      '   and cansz=''' + password + '''');
    if qry1.RecordCount > 0 then
    begin
      ioperid := 0;
      Result := True;
    end
    else
    begin
      errmsg := '错误的登录口令！';
      Result := False;
    end;

    qry1.Close;
    Exit;
  end;

  //默认db认证
  Result := auth_type_db(userid, password);
end;

function Tdmoperator.updatePassword(bianm: Integer;
  newpassword: string): Boolean;
begin
  Result := False;

  try
    if bianm=0 then
    begin
      //$$$超级管理员
      qry1.ExecuteSql(format('update sys_xitcs set cansz=''%s''' +
        ' where cansmc=''%s''', [newpassword, 'g_ap']));
    end
    else
    begin
      qry1.ExecuteSql('update jc_yuang set koul=''' + newpassword + '''' +
        ' where bianm=' + IntToStr(bianm));
    end;
    Result := True;
  except
    on E:Exception do
    begin
      errmsg := '修改密码发生错误:' + e.Message;
    end;
  end;
end;

end.
