program pdwcapp;

uses
  Forms,
  SysUtils,
  Windows,
  Dialogs,
  ShellAPI,
  ubase in '..\..\dwlib\uBase\ubase.pas',
  udbbase in '..\..\dwlib\uBase\udbbase.pas',
  udbconfigfile in '..\..\dwlib\module\udbconfigfile.pas',
  uencode in '..\..\dwlib\module\uencode.pas',
  uglobal in '..\..\dwlib\system\uglobal.pas',
  ukeyvaluelist in '..\..\dwlib\module\ukeyvaluelist.pas',
  ulog in '..\..\dwlib\module\ulog.pas',
  umyconfig in '..\..\dwlib\system\umyconfig.pas',
  umysystem in '..\..\dwlib\system\umysystem.pas',
  updos in '..\..\dwlib\module\updos.pas',
  utypes in '..\..\dwlib\system\utypes.pas',
  udlltransfer in 'dllmodule\dll_interfaces\udlltransfer.pas',
  udllfunctions in 'dllmodule\dll_interfaces\udllfunctions.pas',
  CnDigits in '..\..\dwlib\module\CnDigits\CnDigits.pas',
  utblyuang in '..\global\common\utblyuang.pas',
  ucsmsg in '..\..\dwlib\module\csmsg\ucsmsg.pas',
  udbcheck in '..\..\dwlib\module\dbcheck\udbcheck.pas',
  umyhisreport in '..\..\dwlib\module\report\umyhisreport.pas',
  udbtree in '..\..\dwlib\module\dbtree\udbtree.pas',
  HzSpell in '..\..\dwlib\module\huoqupinyin\HzSpell.pas',
  superobject in '..\..\dwlib\module\superobject.pas',
  udllfuns_dbgridSelfDraw in 'dllmodule\dll_interfaces\udllfuns_dbgridSelfDraw.pas',
  udbgCalcCellColorDispath in '..\..\dllmoudle\dbgridSelfDraw\udbgCalcCellColorDispath.pas',
  udbgselfdraw_dbgid_define in '..\..\dllmoudle\dbgridSelfDraw\udbgselfdraw_dbgid_define.pas',
  udwzip in '..\..\dwlib\module\udwzip.pas',
  pcre in '..\..\dwlib\module\TPerlRegEx\pcre.pas',
  PerlRegEx in '..\..\dwlib\module\TPerlRegEx\PerlRegEx.pas',
  
  uDmBase in '..\..\dwlib\DMBase\uDmBase.pas' {DMBase: Tdatamoudle},
  udmtblbase in '..\..\dwlib\DMBase\udmtblbase.pas' {dmtblbase: Tdatamoudle},
  udmtbl_public in '..\global\common\udmtbl_public.pas' {dmtbl_public: Tdatamoudle},
  udmconn in 'dbconn\udmconn.pas' {dmconn: Tdatamoudle},
  uframeBase in '..\..\dwlib\framebase\uframeBase.pas' {framebase: TFrame},
  uframedbbase in '..\..\dwlib\framebase\uframedbbase.pas' {framedbbase: TFrame},

  ufrmcomm_showmessage in '..\..\dwlib\frmcommon\ufrmcomm_showmessage.pas' {frmcomm_showmessage},
  ufrmcomm_process in '..\..\dwlib\frmcommon\ufrmcomm_process.pas' {frmcomm_process},

  ufrmabout in 'frmabout\ufrmabout.pas' {frmabout},
  ufrmmain in 'frmmain\ufrmmain.pas' {frmmain},
  ufrmlogin in 'frmlogin\ufrmlogin.pas' {frmlogin},

  ufrmbase in '..\..\dwlib\formbase\frmbase\ufrmbase.pas' {frmbase},
  ufrmdbbase in '..\..\dwlib\formbase\frmbase\ufrmdbbase.pas' {frmdbbase},
  ufrmmdichildbase in '..\..\dwlib\formbase\frmbase\ufrmmdichildbase.pas' {frmmdichildbase},
  ufrmdbnavbase in '..\..\dwlib\formbase\frmbase\ufrmdbnavbase.pas' {frmdbnavbase},
  ufrmdbdialogbase in '..\..\dwlib\formbase\frmbase\ufrmdbdialogbase.pas' {frmdbdialogbase},
  ufrmdbselectbase in '..\..\dwlib\formbase\frmbase\ufrmdbselectbase.pas' {frmdbselectbase},

  ufrmimportfromexcel in '..\..\dwlib\module\frmImportExcel\ufrmimportfromexcel.pas',
  ufrmdbgridbase_excel in '..\..\dwlib\formbase\ufrmdbgridbase_excel.pas' {frmdbgridbase_excel},
  udmoperator in '..\global\dmtable\udmoperator.pas' {dmoperator: Tdatamoudle},
  udmxiangm in '..\global\dmtable\udmxiangm.pas' {dmxiangm: Tdatamoudle},
  ufrmxitgl_xiugkoul in 'dllmodule\xitguanl\ufrmxitgl_xiugkoul.pas' {frmxitgl_xiugkoul},
  ufrmxitgl_cansshez in 'dllmodule\xitguanl\ufrmxitgl_cansshez.pas' {frmxitgl_cansshez},
  ufrmxitgl_quanxgl in 'dllmodule\xitguanl\ufrmxitgl_quanxgl.pas' {frmxitgl_quanxgl},
  udmtbl_jiaosdy in '..\global\dmtable\udmtbl_jiaosdy.pas' {dmjiaosdy: Tdatamoudle},
  ufrmmainBG in 'frmmain\ufrmmainBG.pas' {frmmainBG},
  ufrmdbtreebase_mdi in '..\..\dwlib\formbase\frmtreebase\ufrmdbtreebase_mdi.pas' {frmdbtreebaseMDI},
  ufrmcomm_selyuang in 'dllmodule\frmselect\ufrmcomm_selyuang.pas' {frmcomm_selyuang},
  ufrmbumMDI in 'dllmodule\tablemanager\ufrmbumMDI.pas' {frmbumMDI},
  ufrmcomm_selbumyuang in 'dllmodule\frmselect\ufrmcomm_selbumyuang.pas' {frmselbumyuang};
{$R *.res}

var
  hGlobalMutex : THandle;
  dwGlobalMutex_return : DWORD;
  mainhandle : HWND; 
  dllparams : TDllParam;

  i : integer;
  myupdatefilename : string;
  dbserver : string;
  dbuser : string;
  dbpassword : string;
  dbdatabase : string;
begin
  try              
    Application.Initialize;   
    Application.Title := '大维C版开发框架';
    hGlobalMutex := CreateMutex(nil, false, 'dw-tech-capp-framework');
    dwGlobalMutex_return := GetLastError;
    if ERROR_ALREADY_EXISTS = dwGlobalMutex_return then
    begin
      Application.MessageBox('大维C版开发框架！', '系统提示');
      Sleep(1000);
      Exit;
    end; 

    {1、创建主窗体
    }
    Application.CreateForm(Tfrmmain, frmmain);
    Application.CreateForm(Tdmconn, dmconn);

    //创建系统控制对象,并且初始化
    gmysystem := TMySystem.create(@dmconn.conn);
    if not gmysystem.init() then
    begin
      raise Exception.Create('系统初始化失败:' + gmysystem.errmsg);
    end;

    { 登录 }
    MakeDllParams(@dllparams);

    frmlogin := tfrmlogin.Create(nil);
    if not frmlogin.Execute_dll_showmodal(@dllparams) then
    begin
      raise Exception.Create('登录失败:' + frmlogin.errmsg);
    end;
             
    //设置登录员工信息//员工权限
    if not gmysystem.loginyuang.getyuangxx(frmlogin.bianm) then
      raise Exception.Create('获取员工信息失败:' + gmysystem.loginyuang.errmsg);

    //写登录日志
    frmlogin.writelog();

    if assigned(frmlogin) then
      FreeAndNil(frmlogin);
        
    frmmain.init;      
    Application.Run;
  except
    on e:exception do
    begin
      MessageDlg(e.Message, mtWarning, [mbYes], 0);

      //$$$判断是否需要升级？
      if gmysystem.errcode = -999 then
      begin            
        //取升级数据库信息
        if not gmysystem.getDbLoingIngo(gmysystem.dbupdate_server_string,
            dbserver, dbuser, dbpassword, dbdatabase) then
        begin
          gmysystem.showerror('获取升级服务器登录信息失败!');
        end;

        { 2014-11-08:这里采用了新的生机程序pmyhisupdate.exe;
          这个升级程序做了服务器和本地升级版本号是否存在断档的检查点；
        }
        myupdatefilename := ExtractFilePath(Application.ExeName) +
          'pmyupdate.exe';    
        ShellExecute(0, 'open', PChar(myupdatefilename),
          PChar(gmysystem.shengjxh + ' ' + gmysystem.shengjxh_server + ' ' +
              dbserver + ' ' + dbuser + ' ' + dbpassword + ' ' +
              dbdatabase), ' ', SW_SHOW);
      end;          
            
      KillTask('ppdfreview.exe');
      
      if Assigned(gmysystem) then
        FreeAndNil(gmysystem);
                       
      if Assigned(dmconn) then
        FreeAndNil(dmconn);
                          
      if Assigned(frmcomm_showmessage) then
        FreeAndNil(frmcomm_showmessage);

      freeandnil(frmmain);
        
      application.Terminate;
    end;
  end;
  KillTask('ppdfreview.exe');
end.
