//*******************************************************************
//  pmyhisgroup  pdbgridSelfDraw  
//                                          
//  创建: changym by 2012-08-03 
//        changup@qq.com                    
//  功能说明:                                            
//      住院医生站dbg自画函数
//  修改历史:
//                                            
//  版权所有 (C) 2012 by changym
//                                                       
//*******************************************************************

unit udbgselfdraw_zhuyyisz;

interface

uses
  SysUtils,
  DB,
  Dialogs,
  Graphics,
  ADODB,
  Grids,
  DateUtils,
  Messages,
  Windows,
  Wwdbigrd,
  Wwdbgrid,
  UpWWDbGrid, udbgselfdraw_dbgid_define;
                                
//住院医生站-医嘱管理-长期医嘱-编辑dbg；
procedure dbgCalcCellColor_zhuy_yisz_yizgl_changqyz_eddbg(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);

//住院医生站-医嘱管理-临时医嘱-编辑dbg；
procedure dbgCalcCellColor_zhuy_yisz_yizgl_linsyz_eddbg(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);

implementation

//住院医生站-医嘱管理-长期医嘱-编辑dbg
procedure dbgCalcCellColor_zhuy_yisz_yizgl_changqyz_eddbg(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
  //护士已经审核；显示为只读颜色；否则显示为编辑颜色；
  if Field.DataSet.FieldByName('husshbs').AsInteger = 1 then
  begin
    ABrush.Color := StringToColor(clYiz_husYijShenh);
  end
  else
  begin
    ABrush.Color := StringToColor(dbg_edit_color);
  end;

  //医生已经停嘱；显示为停嘱颜色;
  if Field.DataSet.FieldByName('yistzbs').AsInteger = 1 then
  begin
    ABrush.Color := StringToColor(dbg_readonly_color);
  end;
      
  //高亮处理
  if Highlight then
  begin
    ABrush.Color := StringToColor(dbg_highlight_color);
    AFont.Color := clBlue;
  end;

  //组号; 按照组号奇偶数字错开显示不同的颜色
  if SameText(Field.FieldName, 'zuh') then
  begin
    //if ((DayOf(Field.DataSet.FieldByName('kaizsj').AsDateTime) + Field.AsInteger) mod 2 = 0) then
    if (Field.AsInteger mod 2 = 0) then
    begin
      ABrush.Color := RGB($00,$ff,$ff);
    end
    else
    begin
      ABrush.Color := RGB($ff,$69,$b4);
    end;
  end;

  //皮试结果
  if SameText(Field.FieldName, 'pisjg') then
  begin
    case Field.AsInteger of
      0:
      begin

      end;
      1:
      begin

      end;
      2: //皮试未通过
      begin
        AFont.Color := clRed;
      end;
    end;
  end;
end;


//住院医生站-医嘱管理-临时医嘱-编辑dbg；
procedure dbgCalcCellColor_zhuy_yisz_yizgl_linsyz_eddbg(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin    
  //护士已经审核；显示为只读颜色；否则显示为编辑颜色；
  if Field.DataSet.FieldByName('husshbs').AsInteger = 1 then
  begin
    ABrush.Color := StringToColor(clYiz_husYijShenh);
  end
  else
  begin
    ABrush.Color := StringToColor(dbg_edit_color);
  end;

  //医生已经停嘱；显示为停嘱颜色;
  if Field.DataSet.FieldByName('yistzbs').AsInteger = 1 then
  begin
    ABrush.Color := StringToColor(dbg_readonly_color);
  end;

  //临时医嘱，护士已经执行，显示为已执行颜色
  if Field.DataSet.FieldByName('huszxbs').AsInteger = 1 then
  begin                
    ABrush.Color := StringToColor(clYiz_husYijZhix);  
  end;

  //高亮处理
  if Highlight then
  begin
    ABrush.Color := StringToColor(dbg_highlight_color);
    AFont.Color := clBlue;
  end;

  //组号; 按照组号奇偶数字错开显示不同的颜色
  if SameText(Field.FieldName, 'zuh') then
  begin
    if (Field.AsInteger mod 2) = 0 then
    begin
      ABrush.Color := RGB($00,$ff,$ff);
    end
    else
    begin
      ABrush.Color := RGB($ff,$69,$b4);
    end;
  end;

  //皮试结果
  if SameText(Field.FieldName, 'pisjg') then
  begin
    case Field.AsInteger of
      0:
      begin

      end;
      1:
      begin

      end;
      2: //皮试未通过
      begin
        AFont.Color := clRed;
      end;
    end;
  end;
end;

end.
