{ 2014-10-26:似乎废弃的窗体？
}
unit ufrmxitgl_qingksj;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ufrmywbase, Menus, UpPopupMenu, frxClass, frxDesgn, frxDBSet,
  UpDataSource, DB, ADODB, UpAdoTable, UpAdoQuery, ExtCtrls, Buttons,
  UpSpeedButton, UPanel, ComCtrls, UpPageControl, StdCtrls, UpEdit, UpLabel,
  udmtbl_yis, UpCheckBox, CnValidateImage, udmtbl_xitgl, frxExportXLS,
  frxExportPDF;

type
  Tfrmxitgl_qingksj = class(Tfrmywbase)
    p1: TUPanel;
    p2: TUPanel;
    lbl1: TUpLabel;
    lbl2: TUpLabel;
    edtkoul: TUpEdit;
    btnqingksjyz: TUpSpeedButton;
    lbl4: TUpLabel;
    imgyanzm: TCnValidateImage;
    btnqingksj: TUpSpeedButton;
    edtyanzm: TUpEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnqingksjyzClick(Sender: TObject);
    procedure imgyanzmClick(Sender: TObject);
    procedure btnqingksjClick(Sender: TObject);
  private
    //医生对象; 也就是操作员对象;
    dmyis : Tdmtbl_yis;

    //系统管理对象
    dmxitgl : Tdmtbl_xitgl;
    
  procedure cleardata;
  //清空数据

  protected
    function AppendPost() : Boolean; override;

    function Execute_dll_show_before() : Boolean; override;

  public

  end;

var
  frmxitgl_qingksj: Tfrmxitgl_qingksj;

implementation

{$R *.dfm}

function Tfrmxitgl_qingksj.AppendPost: Boolean;
begin
  Result := True;
end;

function Tfrmxitgl_qingksj.Execute_dll_show_before: Boolean;
begin
  inherited Execute_dll_show_before;

  //默认为输入管理员口令面板
  p1.BringToFront;

  //创建医生对象
  dmyis := Tdmtbl_yis.Create(@dllparams);

  //创建系统管理对象
  dmxitgl := Tdmtbl_xitgl.Create(@dllparams);

  Result := True;
end;

procedure Tfrmxitgl_qingksj.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  //销毁医生对象
  FreeAndNil(dmyis);

  //销毁系统管理对象
  FreeAndNil(dmxitgl);

  frmxitgl_qingksj := nil;
end;

procedure Tfrmxitgl_qingksj.btnqingksjyzClick(Sender: TObject);
begin
  inherited;

  //校验输入的管理员口令是否正确

  //口令不允许为空
  if Trim(edtkoul.Text) = '' then
  begin
    baseobj.showwarning('请输入管理员口令！');
    Exit;
  end;

  //验证口令
  if not dmyis.checkPassword('admin', edtkoul.Text) then
  begin
    //管理员口令不正确
    baseobj.showerror('管理员口令不正确,请重新输入！');
    edtkoul.Clear;
    Exit;
  end;

  //口令校验通过, 进入数据清空界面
  p2.BringToFront;
end;

procedure Tfrmxitgl_qingksj.imgyanzmClick(Sender: TObject);
begin
  inherited;

  //重新生成验证码
  imgyanzm.RandomValue;
end;

procedure Tfrmxitgl_qingksj.btnqingksjClick(Sender: TObject);
begin
  inherited;

  //校验验证码是否正确
  if not imgyanzm.ValidateInput(edtyanzm.Text) then
  begin
    baseobj.showwarning('验证码不正确,看不清请单击图片重新获取验证码！');
    edtyanzm.Clear;
    Exit;
  end;

  //开始清空选择类型的业务数据;
  //再次询问
  if not baseobj.showdialog('数据清空提示', '确认清空选择模块的业务数据吗？') then Exit;
  
  //开始清空
  cleardata();
end;

procedure Tfrmxitgl_qingksj.cleardata;
begin
  try
    //启动事务
    dmxitgl.BeginTrans;
    dmxitgl.addspparam_in('@operid', dllparams.mysystem^.loginyuang.bianm); 
    dmxitgl.addspparam_in('@isclearjicsj', dllparams.mysystem^.loginyuang.bianm);
    dmxitgl.sys_yewsjqk;

    //提交事务
    dmxitgl.CommitTrans;

    baseobj.showmessage('业务数据清空成功！');
    edtyanzm.Clear;
  except
    on E:Exception do
    begin
      //回滚事务
      dmxitgl.RollbackTrans;
      baseobj.showerror('清空业务数据失败:' + e.Message);
      Exit;
    end;
  end;
end;

end.
