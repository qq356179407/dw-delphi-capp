inherited frmcxbb_xitgl_caozrz: Tfrmcxbb_xitgl_caozrz
  Left = 124
  Top = 92
  Width = 1097
  Height = 630
  Caption = #31995#32479#25805#20316#26085#24535
  PixelsPerInch = 96
  TextHeight = 14
  inherited rztlbr2: TRzToolbar
    Width = 1081
    ToolbarControls = (
      btnselect
      btnprint
      btnquit)
  end
  inherited dbg: TUpWWDbGrid
    Width = 1081
    Height = 556
    Selected.Strings = (
      'bianm'#9'10'#9#32534#30721#9#9
      'mokmc'#9'18'#9#21151#33021#27169#22359#9'F'
      'chuangkmc'#9'26'#9#25805#20316#31383#21475#9'F'
      'caozyxm'#9'18'#9#25805#20316#20154#9'F'
      'caozsj'#9'21'#9#25805#20316#26102#38388#9'F'
      'caoz'#9'18'#9#25805#20316#9'F'
      'yulzfc1'#9'64'#9#22791#27880#9'F')
  end
  inherited pchax: TUpAdvToolPanel
    Left = 440
    Top = 326
    Width = 507
    object lbl17: TUpLabel [0]
      Left = 295
      Top = 37
      Width = 7
      Height = 14
      Caption = '-'
    end
    inherited pchax_bottom: TPanel
      Width = 503
      TabOrder = 5
      inherited btnselcancel: TUpSpeedButton
        Left = 339
        Top = 5
      end
      inherited btnselyes: TUpSpeedButton
        Left = 415
        Top = 5
      end
      inherited btnselclean: TUpSpeedButton
        Left = 264
        Top = 5
      end
    end
    object chkcaozsj: TRzCheckBox
      Left = 12
      Top = 36
      Width = 89
      Height = 16
      Caption = #25805#20316#26102#38388#65306
      State = cbUnchecked
      TabOrder = 2
      Transparent = True
    end
    object updtcaozsjbegin: TAdvDateTimePicker
      Left = 103
      Top = 33
      Width = 188
      Height = 21
      Date = 43328.962210648150000000
      Time = 43328.962210648150000000
      ImeName = #20013#25991' - QQ'#25340#38899#36755#20837#27861
      Kind = dkDateTime
      TabOrder = 0
      BorderStyle = bsSingle
      Ctl3D = True
      DateTime = 43328.962210648150000000
      Version = '1.0.3.0'
    end
    object updtcaozsjend: TAdvDateTimePicker
      Left = 306
      Top = 33
      Width = 188
      Height = 21
      Date = 43328.962210648150000000
      Time = 43328.962210648150000000
      ImeName = #20013#25991' - QQ'#25340#38899#36755#20837#27861
      Kind = dkDateTime
      TabOrder = 1
      BorderStyle = bsSingle
      Ctl3D = True
      DateTime = 43328.962210648150000000
      Version = '1.0.3.0'
    end
    object chkcaozy: TRzCheckBox
      Left = 12
      Top = 61
      Width = 75
      Height = 16
      Caption = #25805#20316#21592#65306
      State = cbUnchecked
      TabOrder = 4
      Transparent = True
    end
    object edtcaozy: tuprzbuttonedit
      Left = 103
      Top = 58
      Width = 188
      Height = 22
      ImeName = #20013#25991' - QQ'#25340#38899#36755#20837#27861
      TabOrder = 3
      AllowKeyEdit = False
      ButtonKind = bkFind
      AltBtnWidth = 18
      ButtonWidth = 18
      AutoInit = False
      HistoryValueOnOff = True
      FlatStyle = False
    end
  end
  inherited qryreport_m: TUpAdoQuery
    CursorType = ctStatic
    SQL.Strings = (
      'select * from v_sys_riz_caoz')
  end
end
