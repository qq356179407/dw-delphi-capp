unit ufrmmain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, udwzip;

type
  TForm2 = class(TForm)
    btn1: TButton;
    edtfile: TEdit;
    btn2: TButton;
    btn3: TButton;
    dlgOpen: TOpenDialog;
    procedure btn2Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
  private
  public
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

procedure TForm2.btn2Click(Sender: TObject);
begin
  //$$$检测源文件是否存在
  if not FileExists(edtfile.Text) then
  begin
    ShowMessage('文件不存在！');
    exit;
  end;

  //开始压缩
  CompressionFile(edtfile.Text, edtfile.Text+'.dw');  
end;

procedure TForm2.btn3Click(Sender: TObject);
begin       
  //$$$检测源文件是否存在
  if not FileExists(edtfile.Text) then
  begin
    ShowMessage('文件不存在！');
    exit;
  end;

  //开始压缩
  DecompressionFile(edtfile.Text, ExtractFilePath(edtfile.Text) + ExtractFileName(edtfile.Text));
end;

procedure TForm2.btn1Click(Sender: TObject);
begin
  if dlgOpen.Execute then
    edtfile.Text := dlgOpen.FileName;
end;

end.
